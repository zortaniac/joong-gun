import {DefaultTheme, defineConfig } from 'vitepress'
import * as path from 'path'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  head: [['link', { rel: 'icon', href: '/favicon.ico' }]],
  title: "Joong-Gun",
  description: "Taekwon-Do Zwolle, martial arts, vechtkunst, vechtsport, zelfverdediging",
  srcExclude: ['README.md'],
  themeConfig: {
    sidebar: {
      '/over-ons/': sidebarJoongGun()
    }
  },
  outDir: 'dist',
  contentProps: {},
  vite: {
    build: {
      cssCodeSplit: false,
    },
    resolve: {
      alias: {
        '~bootstrap': path.resolve(__dirname, '../node_modules/bootstrap'),
      }
    }
  }
})

function sidebarJoongGun(): DefaultTheme.SidebarItem[] {
  return [
    {
      text: 'Joong-Gun',
      base: '/over-ons/',
      items: [
        { text: 'Over ons', link: 'index' },
        { text: 'Lessen', link: 'lessen' },
        { text: 'Geschiedenis', link: 'geschiedenis' },
        { text: 'Instructeurs', link: 'instructeurs' },
        { text: 'Zwarte banders', link: 'zwarte-banders' },
        { text: 'Dantrainingen', link: 'dantrainingen' },
        { text: 'Samenwerking', link: 'samenwerking' }
      ]
    }
  ]
}
