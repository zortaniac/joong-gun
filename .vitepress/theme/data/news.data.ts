import { createContentLoader } from 'vitepress'
import {copyFile,existsSync, mkdirSync,readFileSync} from 'fs'
import {join, dirname} from 'path'
import {toLocaleDate} from "../helper";

export interface Card {
    title: string;
    url: string;
    excerpt: string
    date: string;
    image: string;
}

declare const data: Card[];
export { data };


export default {
    watch: ['nieuws/**/*.*'],
    async load(watchedFiles:string[]) {
        // copy everything that is not markdown to the public directory
        watchedFiles.filter((file) => !file.endsWith(".md")).forEach((file) => {
            const dir = join('public', dirname(file))
            if (!existsSync(dir)){
                mkdirSync(dir, { recursive: true });
            }
            copyFile(file, join('public', file), (err) => {
                if (err) throw err;
            })
        })
        // create a content loader for the actual markdown files
        return await createContentLoader('nieuws/**/*.md', {
            excerpt: true,
            transform(raw) {
                return raw
                    .map(d => {
                        d.frontmatter.news = true;
                        return d
                    })
                    .map(({ url, frontmatter, excerpt }) => ({
                        title: frontmatter.title,
                        url,
                        author: frontmatter.author,
                        excerpt: frontmatter.excerpt,
                        date: formatDate(frontmatter.date),
                        image: frontmatter.image,
                        news: true
                    }))
                    .filter((d) => d.url != "/nieuws/")
                    .sort((a, b) => b.date.time - a.date.time)
            }
        }).load()
    }
}

function formatDate(raw) {
    const date = new Date(raw)
    return {
        time: +date,
        string: toLocaleDate(date)
    }
}
