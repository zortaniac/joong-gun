const options : Intl.DateTimeFormatOptions = {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
};

/**
 * format date to local date
 */
export function toLocaleDate(date: Date) {
    console.log(date)
    return date.toLocaleDateString('nl-NL', options);
}