// https://vitepress.dev/guide/custom-theme
import Layout from './Layout.vue'
import './style.css'
import './fonts/lato/stylesheet.css'
import './fonts/fira/stylesheet.css'
import "bootstrap/dist/css/bootstrap.css";

export default {
  Layout,
  enhanceApp({ app, router, siteData }) {
    // ...
  }
}

