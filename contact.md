<script setup lang="ts">
  import Greijdanusmap from ".vitepress/theme/components/Greijdanusmap.vue";
</script>

# Contact

<div class="row">
  <div class="col-lg-6">

> Taekwon-Do school Joong-Gun

> Facebook: <a href="https://www.facebook.com/JoongGunZwolle" target="_blank" class="underlined">JoongGunZwolle</a>

> Email: <a href="mailto:joongguntkd@gmail.com" class="underlined">joongguntkd@gmail.com</a>

> Locaties:

<p class="ml-3">* Zwolle (dinsdags): Sportzaal Greijdanus, Campus 5 <Greijdanusmap/></p>
<p class="ml-3">* Zwolle (zaterdags): Judozaal OnCampus Windesheim, Campus 2</p>

  </div>
</div>

