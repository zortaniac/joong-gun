---
home: true
title: Home
---

<div class="row mb-3">
  <div class="col-6 col-sm-12 h1-wrapper">
    <h1 class="mb-md-3">Welkom bij Joong-Gun</h1>
    <span class="d-block d-md-none mb-5">Joong-Gun is een ITF Taekwon-Do school in Zwolle.</span>
  </div>
  <div class="home-image-3-wrapper">
    <div class="black-border">
      <a href="/nieuws/2025/taekwon-do-school-joong-gun-10-jaar.html">
        <img src="/nieuws/2025/joong-gun_10-jaar-home.png" class="blue-border" alt="Taekwon-Do school Joong-Gun bestaat 10 jaar">
      </a>
    </div>
  </div>
  <div class="home-image-2-wrapper">
    <div class="black-border">
      <a href="/nieuws/2025/opening-hidzo-sport.html">
        <img src="/nieuws/2025/opening-hidzo-sport-home.jpg" class="blue-border" alt="Opening Hidzo Sport">
      </a>
    </div>
  </div>
  <div class="home-image-1-wrapper">
    <div class="black-border">
      <a href="/nieuws/2024/zomervakantie.html">
        <img src="/nieuws/2024/zomervakantie-2024.jpg" class="blue-border" alt="Vakantiefoto's 2024">
      </a>
    </div>
  </div>
</div>

<span class="d-none d-md-block">Joong-Gun is een ITF Taekwon-Do school in Zwolle.</span>
Wij vinden het belangrijk dat iedereen zicht *thuis* voelt. Daarom kan iedereen meedoen met de training. Joong-Gun 
is voor jong en oud en elk niveau. Of je nu op zoek bent naar een manier om zelfverdediging te leren, te sparren of 
‘gewoon’ om op een leuke en gezellige manier in beweging te zijn: bij Joong-Gun helpen we je graag met jouw doel. We 
nodigen je van harte uit om vrijblijvend een keer mee te komen doen. Vechtkunst of vechtsport: bij Joong-Gun kan je 
trainen zoals jij dat wilt.

<p class="alert alert-info">
Heb je geen zin of geen energie? Van plezierig sporten krijg je energie!<br />
Heb je energie over? In onze positieve sfeer kan je die energie heerlijk kwijt!
</p>
