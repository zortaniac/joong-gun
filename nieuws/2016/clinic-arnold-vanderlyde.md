---
author: Mark
date: 2016-04-11
title: Clinic Vanderlyde
image: /clinic-arnold-vanderlyde-k.jpg
excerpt: We zijn naar een boksclinic geweest van oud bokskampioen Arnold Vanderlyde
images:
  clinic-arnold-vanderlyde.jpg: 'foto met Arnold vanderLyde, Gretha Veldhuizen en Mark Breet'
---

# Clinic Arnold Vanderlyde
> Vanavond zijn we naar een boksclinic geweest van oud bokskampioen Arnold Vanderlyde. In zijn loopbaan behaalde hij als amateur driemaal brons op de Olympische Spelen: in 1984, 1988 en 1992. Daarnaast was hij driemaal Europees en achtmaal Nederlands kampioen.

De clinic was ontzettend leerzaam en inspirerend. We deden een aantal leuke oefeningen die we niet gauw vergeten.

Na afloop hebben we nog heel even gekletst en het (uiteraard) ook nog even over Taekwon-Do gehad: hoe goed je deze bewegingen ook daar weer bij kunt toepassen. We vinden het geweldig deze man in levende lijve ontmoet te hebben.

