---
author: Mark
date: 2016-04-27
title: Koningsspelen Barneveld
image: /koningsspelen-2016-barneveld.jpg
excerpt: Op 27 april 2016 heeft een delegatie van Joong-Gun meegeholpen met de koningsspelen in Barneveld.
images:
  koningsspelen-2016-barneveld.jpg: circa 30 mensen poseren voor de foto
---

# Koningsspelen 2016 Barneveld
> Op 27 april heeft een delegatie van Joong-Gun meegeholpen met de koningsspelen in Barneveld. Samen met 
> enthousiaste Taekwon-doins vanuit het hele land hebben we de handen ineen geslagen om van de koningsdag op het Gowthorpeplein in Barneveld een geslaagde dag te maken. Het is gaaf om de kinderen lol te zien hebben en om zelf ook lol te maken (ook niet vergeten ;-)). Ook is het gaaf een boel Taekwon-do vrienden weer te ontmoeten.
> Wie weet… volgend jaar?
