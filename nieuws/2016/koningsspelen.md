---
author: Mark
date: 2016-04-22
title: Koningsspelen Zwolle 2016
image: /koningsspelen-2016-zwolle.jpg
excerpt: Workshop Taekwon-Do voor 500 leerlingen tijdens de koningsspelen Zwolle
images:
  koningsspelen-1.jpg: Koningsspelen Zwolle 2016
  koningsspelen-2.jpg: Koningsspelen Zwolle 2016
  koningsspelen-3.jpg: Koningsspelen Zwolle 2016
  koningsspelen-4.jpg: Koningsspelen Zwolle 2016
  koningsspelen-9.jpg: Koningsspelen Zwolle 2016
---

# Koningsspelen Zwolle 2016
> Op 22 april 2016 was Joong-Gun al vroeg aanwezig bij de koningsspelen. Ongeveer 500 leerlingen van de basisscholen in Zwolle Zuid waren hierbij aanwezig. Zij konden verschillende sportclinics van 30 minuten per stuk volgen, waaronder Taekwon-Do.
> De tijd ging razendsnel voorbij. De kinderen waren superenthousiast en hebben hard getraind. Wij kijken terug op een  zeer geslaagde dag!

