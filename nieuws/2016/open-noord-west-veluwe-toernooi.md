---
author: Mark
date: 2016-05-14
title: Open Noord West Veluwe Toernooi
image: /elburg.jpg
excerpt: Op 14 mei 2016 gingen we naar het Open Noord West Veluwe Toernooi
images:
  elburg1.jpg: De deelnemers en coaches van Joong-Gun poseren in gevechthouding met de gewonnen trofeëen
  elburg2.jpg: Jamie zit in scheidsrechterkleren
  elburg3.jpg: Jeremy raakt tijdens het sparren de tegenstander op de helm met zijn voet
  elburg4.jpg: Demi raakt tijdens het sparren de tegenstander op de helm met haar voet
  elburg5.jpg: coach Gretha en Demi poseren met de gewonnen beker van Demi
  elburg6.jpg: coach Mark en Jeremy poseren met de gewonnen beker van Jeremy
  elburg7.jpg: De scheidsrechters wijzen Hester aan als winnaar
  elburg8.jpg: Demi neemt de gewonnen beker in ontvangst
  elburg9.jpg: Tijmen heeft de derde prijs gewonnen
---

# Open Noord West Veluwe Toernooi 
Op 14 mei 2016 reizen we met 6 deelnemers, 2 scheidsrechters en 2 coaches naar Elburg voor het ONWV-Taekwon-Do toernooi. Met de morele ondersteuning van een grote schare supporters op de tribune uit Zwolle en omgeving werden door de deelnemers gestreden op de onderdelen tuls en sparring.

Bij de jeugd was als eerste Tijmen aan de beurt. Hij liet zijn kwaliteiten op het gebied van het lopen van tuls zien en dat resulteerde in het behalen van de derde prijs. Daarna ging ook Demi in haar poule de strijd aan. Na een spannende finale, pakte zij de tweede prijs.
Bij de volwassenen is als eerste Michelle voor Joong-Gun uitgekomen op het onderdeel tuls. Om te kleine poules te voorkomen, is Michelle in een poule geplaatst met een deelnemer die anders in een hogere poule zou vallen. Michelle heeft zich van haar sterke kant laten zien, door tijdens de finale heel strak Chon-Ji tul te lopen. Zij behaalde daarmee de tweede plaats.
Hester heeft haar zenuwen weten te bedwingen in een grote poule en is (weer) boven zichzelf uitgestegen. Celine heeft ook een goede wedstrijd gehad. Hester en Celine grepen net naast de prijzen met een vierde plaats.

Demi deed naast het onderdeel tuls ook mee aan het onderdeel sparring. In een mooie poule heeft zij laten zien dat zij zaken snel oppikt. Na de eerste partij heel netjes te hebben gewonnen, was de finale zinderend. Met een eindscore van 5-6 ging de eerste prijs net aan haar neus voorbij. Iedereen van Joong-Gun is trots op deze tweede prijs van Demi!
De laatste ambassadeur van Joong-Gun die zich liet gelden op het toernooi is Jeremy. Voor Joong-Gun kon de dag dan ook niet mooier worden afgesloten dan met de eerste prijs die hij mee naar huis neemt!

Alle deelnemers zijn weer een ervaring rijker. Joong-Gun is trots op de prestaties van alle deelnemers.

Naast al deze rivaliteit hebben we ook weer een heleboel Taekwon-Do-vrienden mogen begroeten.
Taekwon-Do school Martowirono, bedankt voor de strakke organisatie van deze fantastische dag.
