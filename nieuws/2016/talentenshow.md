---
author: Mark
date: 2016-06-10
title: Talentenshow
image: /talentenshow.jpg
excerpt: Marisa heeft mee gedaan aan een talentenshow op school
images:
  talentenshow4.jpg: Marisa staat op het podium te vertellen
  talentenshow1.jpg: Marisa doet een trap voor
  talentenshow3.jpg: Marisa doet een stoot voor
---

# Talentenshow Marisa
Deze week heeft Marisa mee gedaan aan een talentenshow op school. Wij zijn ontzettend trots op Marisa. Het is knap om alleen op een podium technieken te laten zien. Natuurlijk is dit ook weer een mooie manier om andere kinderen kennis te laten maken met Taekwon-Do. Super gedaan Marisa!
