---
author: Mark
date: 2020-02-1
title: Danexamens 2020
image: /nieuws/2020/danexamens-2020.jpg
excerpt: In mei hebben vier Joong-Gunners hun Danexamen afgelegd in Barneveld
images:
  danexamens.jpg: Mark, Barbara, Jamie en Gretha poseren met instructeur van Schaik in de sportzaal
---

# Danexamens 2020
> Dubbel feest deze week bij Joong-Gun! 
> Joong-Gun is alweer 5 jaar geleden opgericht, een bijzonder moment om even bij stil te staan. Wij vieren dit dinsdag met een ouder/ kindtraining. 
> Ook mogen wij vier derde danhouders bijschrijven; Gretha Veldhuizen, Barbara Kuijper, Mark Breet en Jamie Breunis 
> zijn geslaagd voor hun danexamen.
