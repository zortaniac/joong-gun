---
author: Mark
date: 2020-02-3
title: Joong-Gun bestaat 5 jaar
image: /nieuws/2020/joong-gun-bestaat-5-jaar.jpg
excerpt: Op 3 februari bestaat Joong-Gun 5 jaar
images:
  jubileum7.jpg: de sportzaal is vol kinderen en ouders die rondrennen voor een spel als warming up
  jubileum5.jpg: een vader traint met zijn zoon in de sportzaal
  jubileum6.jpg: een zoon legt zijn vader dingen uit in de sportzaal
  jubileum8.jpg: een vader stoot op een kussen
  jubileum9.jpg: een andere vader stoot op een kussen
  jubileum10.jpg: nog een andere vader stoot op een kussen
  jubileum11.jpg: eindelijk. een moeder die op een kussen stoot
  jubileum12.jpg: de kinderen staan met hun vader of moeder opgesteld om af te groeten
  jubileum13.jpg: de kinderen poseren met hun vader of moeder en de trainers en volwassen studenten
  jubileum2.jpg: Myrica toont de zelf gebakken taart 
  jubileum3.jpg: Celine toont de speciale jubileum taart 
---

# Joong-Gun bestaat 5 jaar
> Joong-Gun is alweer 5 jaar geleden opgericht: een bijzonder moment om even bij stil te staan. 
> Wij vieren dit dinsdag met een ouder-kindtraining. 
> Een superleuke training waarbij de ouders zich van hun meest sportieve kant hebben laten zien.
> De leden hadden een aantal verrassingen in petto: een zelfgebakken taart, én een speciale jubileumtaart!
