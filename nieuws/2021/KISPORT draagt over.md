---
author: Mark
date: 2021-04-11
title: KISPORT draagt over
image: /nieuws/2021/kisport-draagt-locaties-over-sm.jpg
excerpt: KISPORT draagt locaties over aan Taekwon-Do school Joong-Gun
images:
  kisport-draagt-locaties-over.jpg: Inspirerend plaatje van een berg met de tekst 
---

# KISPORT draagt locaties over
> De geboortedag van Taekwon-Do is 11 april. En op zondag 11 april 2021 heeft sahyun Harry van Schaik meegedeeld dat 
> de lessen van KISPORT op de locaties Voorthuizen en Kootwijkerbroek worden overgedragen aan Joong-Gun.
> De locatiehouders en lesgevers van de locaties Voorthuizen en Kootwijkerbroek zijn en blijven boosabums Jørgen van 
> de Bovenkamp, Lars van de Bovenkamp en Joyce Goorhuis.

> Sahyun Harry: “Deze scholen zijn volledig zelfstandig en hebben de locaties in eigen beheer. KISPORT sluit haar 
> deuren en kan met trots zeggen dat Taekwon-Do veilig is voor de volgende generaties.

> Namens KISPORT, de familie van Schaik, alle vrijwilligers en de vele prachtige en mooie leden, die we hebben mogen 
> zien opgroeien, uit de grond van mijn hart als oprichter van KISPORT Martial Academy, bedankt en het ga jullie goed!

> Houd Taekwon-Do daar waar het hoort, op nummer één!”
