---
author: Mark
date: 2021-09-01
title: Buitentrainingen 2021
image: /nieuws/2021/buitentraining-2021_sm.jpg
excerpt: In 2021 trainen we buiten
images:
  buitentraining-2021_1.jpg: Groep studenten staat opgesteld in het park 
  buitentraining-2021_2.jpg: Nick maakt een neerwaartse trap 
  buitentraining-2021_3.jpg: Thomas maakt een hoge gesprongen trap 
---

# Buitentrainingen 2021
> Begin 2021 trainen we buiten en op afstand. Handig dat sabum Bernd en boosabum Gretha en boosabum Jamie één huishouden vormen. We zijn afhankelijk van het weer en soms levert dat mooie plaatjes op:
