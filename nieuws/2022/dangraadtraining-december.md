---
author: Mark
date: 2022-12-11
title: Dangraadhoudertraining
image: /dangraadhoudertraining2022.jpg
excerpt: Joong-Gun organiseerde de eerste dangraadhoudertraining van ITF HQ 
images:
  black-belt-training-2022-12-flyer.jpg: Flyer voor dan graad houder training Hoonhorst 11 december 2022 met grootmeester Peter Sanders meester dana stokhof meester colin bakker en meester ad dekker
  black-belt-training-2022-12a.jpg: de hele groep poseert in gevechthouding
  black-belt-training-2022-12b.jpg: alle mensen van joong-gun poseren in gevechthouding
  black-belt-training-2022-12c.jpg: de groep staat opgesteld
  black-belt-training-2022-12d.jpg: de groep staat opgesteld. foto van achteren
  black-belt-training-2022-12e.jpg: de groep maakt een lage blok
  black-belt-training-2022-12f.jpg: de groep maakt een midden stoot
  black-belt-training-2022-12g.jpg: de groep maakt een midden stoot in L stand
  black-belt-training-2022-12h.jpg: drie groepen trainen hun eigen tuls
  black-belt-training-2022-12i.jpg: de eerste danners krijgen training van meester Ad Dekker
  black-belt-training-2022-12j.jpg: de groep krijgt zelfverdediging van grootmeester Sanders
---

# Eerste dangraadhoudertraining
> In december 2022 hebben we de eerste dangraadhoudertraining van ITF HQ bij Joong-Gun gehad. Bij deze wil ik alle 
> aanwezigen bedanken voor het enthousiasme en de prettige sfeer. Gezamenlijke trainingen moeten niet alleen leerzaam, maar vooral ook heel leuk zijn. Met de mensen van ITF HQ komt dat wel goed. 🤗
> Bedankt Grandmaster Peter Sanders en Masters Colin Bakker en Ad AA Dekker voor het verzorgen van de training. 👊
