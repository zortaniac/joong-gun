---
author: Mark
date: 2023-05-14
title: Danexamens 2023
image: /nieuws/2023/danexamens-2023.jpg
excerpt: In mei hebben 7 Joong-Gunners hun Danexamen afgelegd voor grootmeester Peter Sanders
images:
  danexamens2.jpg: De groep geslaagden poseert met instructeur Brandt en grootmeester Sanders
  danexamens3.jpg: Mark, Barbara, Jamie en Gretha poseren met instructeur Brandt in de sportzaal
  danexamens4.jpg: Lars, Joyce en Jørgen poseren in de sportzaal
  danexamens1.jpg: Grootmeester Peter Sanders examineert 
---

# Danexamens 2023
> Vandaag kwam grootmeester Peter Sanders naar Joong-Gun om examens af te nemen.
> Voor ons was het de eerste keer dat een grootmeester ons examen afnam, dus het was extra spannend.
> 
> Jørgen, Lars en Joyce zijn geslaagd voor hun 2e dan>
> 
> Mark, Jamie, Barbara en Gretha zijn geslaagd voor hun 4e dan en mogen zich daarom Taekwon-Do-instructeur noemen.
> 
> Bedankt, grootmeester Peter Sanders, voor het afnemen van onze examens.
> 
> Bedankt, meester Colin Bakker en meester Ad Dekker, voor de extra aandacht tijdens de zwarte bandtrainingen.
> 
> Bedankt, meester Harry van Schaik, voor alle jaren training.
> 
> Bedankt, instructeur Bernd Brandt, voor talloze trainingen en aanwijzingen die ons hebben voorbereid op deze bijzondere dag.
