---
author: Barbara
date: 2024-8-22
title: Barbara op LinkedIn
image: /barbara-linkedin.jpg
excerpt: Barbara schrijft op LinkedIn over 30 jaar ITF - Taekwon-Do-beleving
images:
  barbara-linkedin-l.jpg: Screen shot van het Linked In artikel
---

# 30 jaar ITF - Taekwon-Do-beleving

> Half augustus 1994 startte ik mijn training bij Taekwon-Do School Koryo Amsterdam (nu: Stichting Koryo). Taekwon-Do is een Koreaanse zelfverdedigingssport bestaande uit veel traptechnieken. Ik was 23 jaar en wilde weerbaarder in het leven staan.  Ik ging er blanco in en was een ijverige leerling. Toen ik na mijn eerste examen een gele streep op mijn witte band ontving was ik verkocht.

> Taekwon-Do kent een Oosterse doctrine, waarbij de gradatie van de band je technische niveau weergeeft, en tevens je plek duidt in de hiërarchie. Wij beoefenden de traditionele, semi-contact stijl, welke valt onder de ITF,  de International Taekwon-Do Federation. De grondlegger is generaal Choi Hong-Hi (9 november 1918 - 15 juni 2002). Na zijn overlijden is de ITF wereldwijd versplinterd in meerdere organisaties.

> Ik verrichtte vrijwilligerswerk voor de school, hand- en spandiensten. Een jaar later werd dat omgezet naar een banenpoolplaats. Zo heb ik me professioneel kunnen ontwikkelen. Main core business was het verzorgen van introductielessen  in het basisonderwijs. Ik was heel bevlogen. Op het succes volgde prestige. Meer projecten, en het naar buiten treden van de school. Door ambitie gedreven vervaagde mijn passie voor de sport.  Je kracht is je valkuil.

> Maar Taekwon-Do kruipt in je bloed. Het is een dynamische sport, waarbij je piekt rond je 28e jaar. Op die leeftijd had ik net mijn zwarte band. Koreanen beginnen al op jonge leeftijd met Taekwon-Do, als onderdeel van het bewegingsonderwijs op school en later in militaire dienst. De opbouw van de sport is daarop afgestemd. Als je op latere leeftijd begint, loop je qua ontwikkeling een beetje achter de feiten aan. Op individueel niveau heeft het evenwel een grote meerwaarde.

> Na een periode van onderbreking vervolgde ik mijn weg  bij Taekwon-Do School Uhuru Amsterdam (nu: Evolution Sports). Ik moest erg wennen aan de andere  methodieken, maar dat was wel heel leerzaam. Ik werd gematcht als examenpartner. Dat was een goede binnenkomer om de draad weer op te pakken en ik vond er mijn vaste trainingsmaatje. Ik wilde mijn hobby terug, maar was nog  zoekende naar een richting. Die bood zich aan in 2013; De Tultour.

> Tuls zijn stijlpatronen die genoemd zijn naar personen of gebeurtenissen uit de Koreaanse geschiedenis. Tuls lopen is mijn favoriete onderdeel. Het geeft veel energie. Daarnaast vond ik de culturele achtergrond van Taekwon-Do altijd al fascinerend doch abstract. De Tultour was een culturele  rondreis door Korea. Ik werd gematcht aan Taekwon-Do beoefenaars uit Zwolle. Wij ontmoetten elkaar voor het eerst in de wachtrij op Schiphol. Er werd getraind op inspirerende plaatsen. Ineens viel het kwartje. Ik kan het niet uitleggen. Je moet er geweest zijn om het te ervaren.

> Heden train ik bij Taekwon-Do School Joong-Gun in Zwolle. In 2020 deden wij examen voor de derde Dan. Het was 20 jaar na mijn laatste examen dus ik zag er best tegenop. Onderdeel van dit examen was het schrijven van een scriptie over een Taekwon-Do gerelateerd onderwerp. Dat zat al lang in de pen. Het was een  come back naar mezelf.

> Als je begint in Taekwon-Do begin je met respect. Buigen en groeten met "Taekwon" - voet/vuist. Taekwon leraar, Taekwon groep, Taekwon partner, Taekwon Dojang. De -Do -de weg-  is  wat vager, en laat zich moeilijker omschrijven. De weg kent in ieder geval twee interpretaties. Een recente interpretatie is het morele compas, de Do, de  Do's en de Don'ts. Die verschillen per individu. De traditionele interpretatie is de  levensweg. Taekwon-Do vormt je leven, en je leven vormt Taekwon-Do.

> Terugkijkend op mijn levensweg zie ik een Taekwon-Do carrière waarbij prestatiedrang heeft  plaatsgemaakt voor kameraadschap. Ik train met plezier, maar het liefst in de beslotenheid van de school. Ik ben nu Sabum - leraar. Een nieuwe fase. Voorbeeldfunctie en kennisoverdracht krijgen meer nadruk. Persoonlijke doelen stellen;  vormgeving en zingeving liggen besloten in de toekomst. Ging het trouwens niet om de weg daarnaartoe? Zolang ik mijn tuls kan blijven lopen ga ik sowieso door. Taekwon-Do heeft geen einddatum.

> Taekwon!

> http://taekwondozwolle.nl/
