---
author: Mark
date: 2024-2-27
title: Barbara Special
image: /barbara2024.jpg
excerpt: Ingezonden stuk van Barbara 
images:
  barbara.jpg: Barbara poseert met Gretha
---

# Barbara Special
> Ik ben Barbara Kuijper en train ITF Taekwon-Do sinds 1994. Ik heb Gretha en Jamie leren kennen tijdens de tultour in Zuid-Korea in 2013. 
> Wij kwamen van verschillende scholen; zij uit Zwolle en ik uit Amsterdam. Het was een goede match! 
> Kort daarna heb  ik ook Mark en Margot leren kennen, en ook hun mocht ik erg graag. 
> Vanaf de oprichting van Joong-Gun gingen zij als een speer. In 2018 heb ik me bij de school aangesloten. 
> Ik ben blij dat wij gezamenlijk optrekken in onze ontwikkeling. Joong-Gun is professioneel, betrokken en gezellig.
