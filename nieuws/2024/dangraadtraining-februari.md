---
author: Mark
date: 2024-2-4
title: Dangraadhoudertraining
image: /dangraadhoudertraining2024.jpg
excerpt: Joong-Gun organiseerde een dangraadhoudertraining van ITF HQ in februari 2024
images:
  black-belt-2024-2-flyer.jpg: Flyer voor dan graad houder training
  black-belt-2024-2-groep.jpg: de hele groep poseert in gevechthouding
  black-belt-2024-2-training1.jpg: de hele groep poseert in gevechthouding
  black-belt-2024-2-training2.jpg: de groep staat opgesteld
  black-belt-2024-2-training3.jpg: sabum Mark leert een techniek
---

# Dangraadhoudertraining
> In februari 2024 hebben Jørgen, Lars en Joyce een dangraadhoudertraining van ITF HQ in Kootwijkerbroek georganiseerd.
> De training werd goed bezocht door clubs uit het heel Nederland en uit Duitsland en België.
> Er was aandacht voor het doceren van traptechnieken, voor zelfverdediging en hoge tuls.
> De training werd gegeven door grootmeester Peter Sanders, meester Colin Bakker, meester Ad Dekker en instructeur Bernd Brandt.
> Bedankt!
