---
author: Mark
date: 2024-01-23
title: Examenfoto's
image: /examen-volwassenen-juni.jpg
excerpt: Foto's van examens
images:
  daan-en-margot-met-certificaat.jpg: daan en margot poseren met hun behaalde certificaat
  examen-volwassenen-juni.jpg: De volwassenen poseren vrolijk en zwaaien met hun behaalde certificaat
  nick-breekt-plank.jpg: Nick van 12 jaar breekt de zwarte plank met een zijwaartse trap
---

# Examenfoto's
> Er zijn leuke foto's gemaakt tijdens de examens.
