---
author: Mark
date: 2024-04-9
title: Lerarentraining op de Lassus Campus 
image: /lassus.jpg
excerpt: We hebben training gegeven aan een aantal leraren op de Lassus Campus in Zwolle.
images:
  lassus1.jpeg: zes leraren stoten op stootkussentjes
  lassus3.jpeg: zeven leraren stoten op stootkussentjes en Jamie legt uit
  lassus2.jpeg: Gretha en Jamie en de leraren poseren in gevechthouding
---

# Lerarentraining op de Lassus Campus
> Gretha en Jamie hebben een aantal leraren op de Lassus Campus in Zwolle een Taekwon-Dotraining gegeven. 
> De leraren hebben een inleiding in de theorie van Taekwon-Do gehad. 
> Daarnaast zijn ze uiteraard praktisch uitgedaagd en is er goed gezweet.
