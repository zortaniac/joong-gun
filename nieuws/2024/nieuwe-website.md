---
author: Mark
date: 2024-01-15
title: Nieuwe website 2024
image: /nieuwe-website.jpg
excerpt: Er is een nieuwe website
images:
  nieuwe-website.jpg: gedeelte van een screenshot van de nieuwe website begin 2024
---

# Nieuwe website 2024
> Dit jaar beginnen we ook met het werken aan de nieuwe website. Hannes, Joey en Mark zijn hier hard mee bezig.
> Het oude nieuws wordt nog overgezet en er zijn nog heel veel plannen :-).
