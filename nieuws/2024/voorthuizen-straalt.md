---
author: Mark
date: 2024-4-13
title: Voorthuizen Straalt Demonstratie
image: /voorthuizen-straalt-taekwondo-demonstratie.jpg
excerpt: Joong-Gun geeft een Taekwon-Dodemonstratie op Voorthuizen Straalt 
images:
  voorthuizen-straalt3.jpg: de studenten en trainers staan opgesteld op het plein in Voorthuizen
  voorthuizen-straalt1.jpg: team tul lopen in een ster vorm
  voorthuizen-straalt2.jpg: team tul lopen
  voorthuizen-straalt4.jpg: de studenten lopen een andere tul
  voorthuizen-straalt5.jpg: groene banders in actie met traptechnieken op grote kussens
  voorthuizen-straalt6.jpg: blauwe banders in actie met traptechnieken op grote kussens
---

# Voorthuizen Straalt 2024 Demonstratie
> Op 13 april geeft Joong-Gun een demonstratie op Voorthuizen Straalt. Op twee momenten op de dag laten de trainers 
> en 9 studenten Taekwon-Dotechnieken en tuls zien op het plein in Voorthuizen. Het publiek en het zonnige weer 
> dragen ook bij aan het succes van deze demonstratie.  
