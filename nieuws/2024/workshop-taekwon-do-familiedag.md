---
author: Mark
date: 2024-4-23
title: Workshop familiedag
image: /workshop-taekwondo-op-familiedag.jpg
excerpt: Sabum Mark gaf een workshop Taekwon-Do op een familiedag
images:
  workshop-taekwon-do-familiedag1.jpg: één familielid slaat op een stootkussen dat wordt vastgehouden door instructeur mark in taekwondo kleren
  workshop-taekwon-do-familiedag2.jpg: twee familieleden slaan op een stootkussen dat wordt vastgehouden door instructeur mark in taekwondo kleren
  workshop-taekwon-do-familiedag3.jpg: instructeur mark legt iets uit aan 5 familieleden
  workshop-taekwon-do-familiedag4.jpg: instructeur mark legt iets uit aan 5 familieleden
---

# Workshop Taekwon-Do
> De familie van (sabum) Mark heeft een familiedag en heeft Mark uitgenodigd om een workshop Taekwon-Do te geven.
> De familieleden kunnen uit een aantal workshops kiezen.
> Fijne bijkomstigheid is het mooie weer, waardoor we lekker buiten aan de slag kunnen.
> Er wordt lustig op los gestoten en getrapt, maar de tijd gaat erg snel als je lol hebt. 
> De _dinner gong_ gaat, hetgeen het einde van deze Taekwon-Doworkshop inluidt. 
> Moe en voldaan en gelaafd aan Taekwon-Dokennis gaat de familie richting het eten.
