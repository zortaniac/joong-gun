---
author: Mark
date: 2024-9-10
title: Vakantiefoto's!
image: /zomervakantie-2024.jpg
excerpt: In de zomervakantie zijn er allerlei leuke foto's gemaakt. Check ze uit.
images:
  shauney.jpg: Shauney maakt een zijwaartse trap bovenop een berg
  bram-yopt.jpg: Bram springt met een zijwaartse trap het zwembad in
  mark-yopt.jpg: Mark maakt een zijwaartse trap op een berg
  bernd-gretha-digutja-makgi.jpg: Bernd en Gretha in dobok met een digutja makgi op de heide
  bernd-gretha.jpg: Bernd en Gretha in dobok op de heide
  barbara-bituro.jpg: Barbara maakt een bituro chagi op de heide
  jamie-celine.jpg: Jamie en Celine maken een digutja makgi op de heide
  bernd-barbara-kyocha-makgi.jpg: Bernd en Barbara poseren met een kyocha makgi op de heide
  barbara-op-de-heide.jpg: Barbara op de heide
  bernd-barbara.jpg: Bernd en Barbara poseren op de heide
  joey-jack-sang-sonkal.jpg: Joey en Jack staan met een sang sonkal op de heide
  bernd-jay.jpg: Bernd blokt een yop van Jay op de heide
  jay-yopt-op-de-heide.jpg: Jay maakt een zijwaartse trap op de heide
  bernd.jpg: Bernd in dobok op de heide
  bernd-sonbadak.jpg: Bernd maakt een sonbadak makgi op de heide
  jay-bob.jpg: Jay en Bob op de heide
  joey-jack-sonkal.jpg: Joey en Jack met een sonkal
  joey-jack-kyocha-makgi.jpg: Joey en Jack met een kyocha makgi
  joey-jack.jpg: Joey en Jack in dobok op de heide
  bob-palkup-taerigit.jpg: Bob maakt een elleboogslag
  bob-yopt.jpg: Bob maakt een zijwaartse trap op de heide
  jay-dollyot.jpg: Jay maakt een rondwaartse trap op de heide
  bernd-celine-en-jay-op-de-heide.jpg: bernd celine en jay in hanulson junbi sogi op de heide
  bernd-en-merel.jpg: Bernd en Merel in guburyo
  merel-op-de-heide.jpg: Merel valt Bernd aan
  bob-noollo-makgit-op-de-heide.jpg: Bob met een drukblok
  jamie-biturot-op-de-heide.jpg: jamie met een bituro
  jamie-palmok-daebi-makgit-op-de-heide.jpg: Jamie in een gevechtshouding
  jamie-yopt-op-de-heide.jpg: Jamie met een zijwaartse trap
  kwang-gae-op-de-heide.jpg: 6 zwarte banders in hemelhand stand op de heide
  heideroosjes.jpg: Joong-Gun groepsfoto in dobok op de heide
  heide.jpg: Joong-Gun groepsfoto in dobok op de heide
  celine.jpg: Celine maakt een zijwaartse trap in haar trouwjurk

---

# Zomervakantie 2024
> In de zomervakantie zijn er allerlei leuke foto's gemaakt. Check de laatste foto!
