---
author: Mark
date: 2025-2-8
title: Hidzo Sport
image: /nieuws/2025/opening-hidzo-sport-nieuws.jpg
excerpt: Joong-Gun Kootwijkerbroek en Voorthuizen gaat verder als Hidzo Sport. 
images:
  opening-hidzo-sport.jpg: flyer met daarop opening hidzo sport
---

# Joong-Gun Kootwijkerbroek en Voorthuizen gaat verder als Hidzo Sport.

> Joyce, Jørgen en Lars hebben hun Taekwon-Do-activiteiten in Kootwijkerbroek en Voorthuizen voortgezet onder de 
> paraplu van Joong-Gun vanaf 11 april 2021. Maar vanaf 8 februari gaan zij verder onder eigen naam: Hidzo Sport. De 
> naam is voor Nederlandse Taekwon-Doin een bekend acrostichon. Deze naam geeft weer, dat Taekwon-Do bij deze drie 
> verder gaat dan stoten en trappen. 'Wij van Joong-Gun' wensen hun veel hidzo op hun pad.
> Joyce, Jørgen en Lars blijven zelf wel trainen bij Joong-Gun.
> 
> # [Hidzo website](https://www.hidzosport.nl)
