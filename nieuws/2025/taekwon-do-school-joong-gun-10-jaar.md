---
author: Mark
date: 2025-2-3
title: Joong-Gun 10 jaar
image: /nieuws/2025/joong-gun_10-jaar-nieuws.png
excerpt: Taekwon-Do school Joong-Gun bestaat 10 jaar 
images:
  joong-gun_10-jaar.png: Taekwon-Do school Joong-Gun bestaat 10 jaar 
---

# Taekwon-Do school Joong-Gun bestaat 10 jaar

> Op 3 februari 2025 bestaat Taekwon-Do school Joong-Gun 10 jaar.
