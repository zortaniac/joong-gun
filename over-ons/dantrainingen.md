# Zwarte bandertrainingen

Wij vinden het belangrijk om zelf te blijven trainen, om goed training te kunnen blijven geven. 
Daarnaast houden we ervan anderen te ontmoeten. 
Joong-Gun heeft dan ook al vaak de zwarte-bandertraining van ITF HQ Nederland georganiseerd.

Dit is een training waarop alle zwarte banders van ITF HQ (en dus Joong-Gun) op hun eigen niveau worden getraind 
door Taekwon-Do(groot-)meesters. 

<div class="row">
  <div class="col-md-4">
    <a href="/nieuws/2022/dangraadtraining-december.html">
      <h3 class="unstyled-link">Dangraadtraining december 2022</h3>
      <img src="/nieuws/2022/black-belt-training-2022-12-flyer.jpg" alt="flyer voor de training 2022">
    </a> 
  </div>
  <div class="col-md-4">
    <a href="/nieuws/2024/dangraadtraining-februari.html">
      <h3 class="unstyled-link">Dangraadtraining februari 2024</h3>
      <img src="/nieuws/2024/black-belt-2024-2-flyer.jpg" alt="flyer voor de training 2024">
    </a> 
  </div>
</div>
