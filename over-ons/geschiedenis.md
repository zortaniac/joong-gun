<div class="row">
<div class="col-lg-8">

# Geschiedenis Joong-Gun

<img src="./ahn-joong-gun.jpg" class="left-50" alt="foto van Ahn Joong Gun">

Binnen Taekwon-Do kennen we 24 tuls, of stijlpatronen: één voor elk uur van de dag. Eén dag staat voor één 
mensenleven. Elke tul heeft een betekenis met betrekking tot de geschiedenis van het herkomstland van Taekwon-Do: Korea.

Joong-Gun is de zesde tul binnen Taekwon-Do en is vernoemd naar de patriot en vrijheidsstrijder Ahn Joong-Gun (2 
september 1879 – 26 maart 1910). Hij vocht tegen de Japanse overheersing in Korea. Hij heeft dit uiteindelijk met 
zijn leven moeten bekopen, maar hij leeft voort in de harten van alle Koreanen en wordt in heel Korea geëerd voor 
zijn heldhaftige daden.

Ahn Joong-Gun vermoordde op 26 oktober 1909 de Japanse gouverneur-generaal Hiro Bumi Ito. Hij werd opgepakt en werd 
op 2 november 1909 in de Lushun gevangenis in China geplaatst. Hij verbleef daar 114 dagen en werd daar op 26 maart 
1910 geëxecuteerd.

<img src="./ahn-joong-gun-kaligrafie.jpg" style="float:right;clear:none;" alt="kalligrafie van Ahn Joong Gun">
Tijdens zijn gevangenschap heeft hij veel tijd besteed aan zijn kalligrafische werken, 
waarvan een van de bekendste  op de afbeelding hier rechts te zien is. De karakters betekenen: ‘’Tenzij men elke dag 
leest, groeien er doornen in de mond.’’

In de zomer van 2013 zijn wij afgereisd naar Zuid-Korea om deel te nemen aan de Tul Tour, waarbij we historische 
plaatsen hebben bezocht die betrekking hebben tot de geschiedenis van Korea en de 24 tuls binnen Taekwon-Do. Zo 
bezochten wij ook de Joong-Gun Memorial Hall, die is gewijd aan patriot Ahn Joong-Gun. Wij waren zeer onder de 
indruk hoe hij zijn leven heeft gewijd aan de vrijheid van Korea.

<img src="./ahn-joong-gun-standbeeld.jpg" class="left-50" style="clear: none;" alt="Gretha Veldhuizen en Jamie Breunis
bij een standbeeld van Ahn Joong-Gun in Korea. ">

Onze Taekwon-Do school dankt haar naam aan Ahn Joong-Gun, omdat wij voor vrijheid en gerechtigheid staan, net als 
hij. Deze waarden refereren ook aan twee zinnen uit de eed van Taekwon-Do, namelijk *“Ik zal een voorvechter zijn 
voor vrijheid en gerechtigheid.”* en *“Ik zal helpen bouwen aan een betere wereld.”*

Foto: Gretha Veldhuizen en Jamie Breunis staan bij een standbeeld van Ahn Joong-Gun in Korea. 

</div></div>
