# Over Joong-Gun

<div class="row">
  <div class="col-sm-4 col-md-3 col-lg-2">
  <img src="/over-ons/buitentraining.jpg" alt="buitentraining" class="thumbnail">
  </div>
  <div class="col-sm-8 col-md-9 col-lg-4">

Wil je een keer vrijblijvend ontdekken wat Taekwon-Do is, maar heb je nooit de stap durven zetten om eens te gaan 
kijken? Ben je op zoek naar een nieuwe vriendenclub, heb je zin om lekker actief bezig te zijn en plezier te hebben 
terwijl je sport? Heb je nog nooit aan sport gedaan? Taekwon-Do is voor en van iedereen, van jong tot oud. Bij ons 
hoef je jezelf niet te bewijzen en bij ons hoef je geen ervaring met martial arts te hebben om mee te kunnen doen.

</div>
  <div class="col-sm-4 col-md-3 col-lg-2">
  <img src="/over-ons/international-taekwon-do-federation-headquarters-korea-zwolle.jpg" alt="logo van international taekwondo 
federation headquarters korea" class="thumbnail">
  </div>
  <div class="col-sm-8 col-md-9 col-lg-4">

Bij Joong-Gun wordt training gegeven door Bernd Brandt, Mark Breet, Gretha Veldhuizen en Jamie Breunis. Alle 
trainers van Joong-Gun zijn erkend door de International Taekwon-Do Federation Headquarters Korea. ITF HQ is een van 
de vier ITF groepen.

<br style="clear:both" />

</div>
  <div class="col-sm-4 col-md-3 col-lg-2">
  <img src="/over-ons/joong-gun-logo.png" alt="logo van taekwondo school joong-gun" class="thumbnail">
  </div>
  <div class="col-sm-8 col-md-9 col-lg-4">

Een mooi voorbeeld van de werkwijze bij Joong-Gun is de totstandkoming van het Joong-Gun logo. Eerst is er een
brainstorm-sessie geweest, waarbij iedereen ideeën heeft ingebracht. Daarna zijn er door Mark en Jamie
(onafhankelijk van elkaar) enkele ideeën grafisch uitgewerkt. Hierop is door iedereen gereageerd en er is een logo
uitgekozen. Na enig fijnslijpen is het logo gezamenlijk goedgekeurd.


</div>
  <div class="col-sm-4 col-md-3 col-lg-2">
  <img src="/over-ons/jungshin-150x150.jpg" alt="koreaanse karakters voor hoffelijkheid integriteit doorzettingsvermogen zelfbeheersing en ontembare geest" class="thumbnail">
  </div>
  <div class="col-sm-8 col-md-9 col-lg-4">

Joong-Gun wil handelen volgens de principes van ITF Taekwon-Do:<br>
1. hoffelijkheid<br>
2. integriteit<br>
3. doorzettingsvermogen<br>
4. zelfbeheersing en<br>
5. de zg. ontembare geest.

</div>
</div>

<style>
.thumbnail {
  margin: 1rem 2rem 1rem 0;
}
@media (min-width: 768px) {
.thumbnail {
  margin: 0 2rem 2rem 0;
}
}
</style>
