# Joong-Gun instructeurs

Een korte introductie van de trainers van Joong-Gun.

<img src="./bernd-brandt_beach.jpg" alt="Bernd Brandt maakt een gesprongen dubbele trap op het strand" class="left-50">

## Bernd Brandt

Sabum Bernd is een instructeur die zijn sporen heeft verdiend in Duitsland. Hij heeft daar jaren Taekwon-Do- en Krav 
Maga-training gegeven aan tientallen studenten.

Hij is een no-nonsense trainer met veel kennis over de toepassing van de technieken.

Zijn Dan VI-graduatie heeft hij jaren geleden gehaald onder grootmeester Peter Sanders.

<br style="clear: both;">
<img src="./gretha-veldhuizen_beach.jpg" alt="Gretha Veldhuizen maakt een zijwaartse trap op het strand" 
class="right-50">

## Gretha Veldhuizen

Sabum Gretha is een ware Taekwon-Doholic die het liefst over niets anders praat. Haar weg heeft al naar Polen, China 
en Korea geleid en ze heeft Taekwon-Docontacten over de hele wereld. Het liefst traint ze aan zee of in de sneeuw.

Gretha is actief voor de club en voor Taekwon-Do sinds dat ze begon met trainen. Ze vervulde meerdere posities op 
club-niveau en ze zat in het Do-comité voor ITF Headquarters Nederland.

Gretha: “Taekwon-Do is voor iedereen!”

<br style="clear: both;">
<img src="./jamie-breunis_beach.jpg" alt="Jamie Breunis maakt een zijwaartse trap op het strand" 
class="left-50">

## Jamie Breunis

Sabum Jamie traint al Taekwon-Do van jongs af aan. Haar weg heeft o.a. naar China, Korea en Australië geleid.

Mede door haar studie is zij een prachtige bundeling van professionaliteit en spontaniteit.

<br style="clear: both;">
<img src="./mark-breet_park.jpg" class="right-50" alt="Mark Breet maakt een gesprongen dubbele trap in het park">

## Mark Breet

Sabum Mark is een enthousiaste instructeur die zelfs na jarenlang lesgeven elke training een unieke draai te weet te 
geven. Hij moedigt jong en oud aan om nieuwe dingen te blijven proberen en zet zich ook achter de schermen in voor 
de club. Of het nu binnen de dojang is of daarbuiten, met Mark zul je je nooit vervelen.

<br style="clear: both;">

<br style="clear: both;">
<img src="./team-joong-gun_met-barbara.jpg" class="width-100" alt="Jamie, Mark, Bernd, Gretha én Barbara">

<br style="clear: both;">
Toegift: Jamie, Mark, Bernd, Gretha én Barbara
<p>&nbsp;</p>
