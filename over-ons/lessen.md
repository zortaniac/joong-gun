<script setup lang="ts">
  import Greijdanusmap from "../.vitepress/theme/components/Greijdanusmap.vue";
</script>

# Handige info

Hier volgt handige informatie over de trainingen:

## Proefles
Je bent van harte uitgenodigd voor een laagdrempelige, vrijblijvende proefles. Neem dan sportkleren mee.

## Lessen Zwolle
Dinsdag 18:00-19:00 uur, Sportzaal Greijdanus, 6 tot 14 jaar <Greijdanusmap/><br />
Dinsdag 19:00-20:30 uur, Sportzaal Greijdanus, 14 jaar en ouder <Greijdanusmap/><br />
Zaterdag 10:00-11:00 uur, Judozaal Windesheim 6 jaar en ouder

## Contributie
€ 17,50 per maand (10 tot 14 jaar, 1x/wk trainen)

€ 27,50 per maand (10 tot 14 jaar, 2x/wk trainen)

€ 20,- per maand (14 jaar en ouder, 1x /wk trainen)

€ 30,- per maand (14 jaar en ouder, 2x/wk trainen)
