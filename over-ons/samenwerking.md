<div class="row">
<div class="col-lg-8">

# Samenwerking

![logo international taekwondo federation headquarters korea](international-taekwon-do-federation-headquarters-korea-zwolle.jpg)
De trainers van Joong-Gun zijn aangesloten bij 
Internationale Taekwon-Do Federatie Headquarters Korea (ITF HQ-Korea).
Dit is een administratieve, coördinerende en ondersteunende instantie in het thuisland van Taekwon-Do, die erop 
gericht is om alle ITF beoefenaars, supporters en sympathisanten wereldwijd te dienen, ongeacht religie, ras, 
nationale of ideologische grenzen en organisatorische voorkeuren.

<br style="clear:both;">

![logo jeugd fonds sport](jfs-logo.png)
Joong-Gun is aangemeld bij het Jeugdfonds Sport, dat sportkansen creëert voor kinderen van 4 tot 23 jaar die om 
financiële redenen geen lid kunnen worden van een sportvereniging.
Een leerkracht, jeugdhulpverlener, schuldhulpverlener, sportbuurtcoach, huisarts of een maatschappelijk werker kan 
online een aanvraag voor een kind indienen. Als de aanvraag wordt goed gekeurd, betaalt Jeugdsportfonds de 
contributie voor de sportschool voor het kind.

</div></div>
