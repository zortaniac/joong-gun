# Zwarte banders
De zwarte band is voor veel mensen een doel, zodra (of zelfs voordat) ze een vechtkunst gaan beoefenen. Er wordt ook 
verondersteld, dat iemand met een zwarte band een meester is. De ‘legendarische’ band zou het hoogst haalbare zijn 
en heeft voor sommigen een haast mythische, bovenmenselijke status. Boeken en films hebben daar onder andere aan 
bijgedragen.

Bij Taekwon-Do krijg je, als je de zwarte band haalt, de titel “assistent instructeur”. Als je nog een aantal jaren 
verder traint, kan je “instructeur” worden. En als je daarna nóg een heel aantal jaren verder traint, kan je 
“meester” worden. Zo bezien is het halen van een zwarte band een mijlpaal op de reis, niet het doel. Na het halen 
van de band ben je nog steeds dezelfde als de dag ervoor!

Het bereiken van die mijlpaal kost inzet, training, studie, geduld en doorzettingsvermogen. Daarom bouwt de training 
wel aan jouw karakter en aan die eigenschappen. Niet iedereen heeft die inzet, dat geduld of dat 
doorzettingsvermogen. Slechts enkelen die aan een vechtkunst beginnen, bereiken de mijlpaal van een zwarte band ook. 
Daarom is een zwarte band echt wel iets om trots op te zijn. Daarom ben je tóch een ander mens dan voordat je aan 
Taekwon-Do begon.

![Zwarte banders van Joong-Gun](zwarte-banders-joonggun.jpg)

<br style="clear:both;" />

Hierboven een foto die gemaakt is na de landelijke zwartebandertraining die georganiseerd is door Joong-Gun. Van 
Joong-Gun zijn op deze foto te zien: Jørgen, Jay, Hester, Margot, Bernd, Joyce, Gretha, Barbara, Jamie, Mark en Lars.
Daan was deze dag helaas verhinderd. Daarnaast staan op de foto: meester Dekker, grootmeester Sanders en meester Bakker.
