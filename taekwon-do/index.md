# Taekwon-Do 

<div class="row">
  <div class="col-md-6">
Taekwon-Do is een universele vechtkunst die gebaseerd is op wetenschap, kracht en techniek en is ontworpen en 
opgericht door de Koreaanse generaal Choi Hong Hi.

Taekwon-Do wordt nog steeds in het leger getraind. Voor burgers is het vaak een 
levensstijl van zelfdiscipline, zelfvertrouwen en zelfbeheersing, een sport, een manier om mooie 
technieken uit te kunnen voeren of een manier om zelfverdediging te leren. 
De Internationale Taekwon-Do Federatie (ITF-bond) is gesticht is door de generaal.
  </div>
</div>

## Tuls
Twee mini-applicaties om willekeurige tuls te prikken die je moet leren:

<a class="btn btn-outline-primary mr-3 mb-3" href="tulpicker.html">Tulprikker</a>
<a class="btn btn-outline-primary mr-3 mb-3" href="auto-tulpicker.html">Automatische tulprikker</a>

## Koreaanse uitspraak
> Om de Koreaanse uitspraak van enkele bekende Taekwon-Dotermen te horen, klik op de volgende links en dan op de luidspreker

<a class="btn btn-outline-primary mr-3 mb-3" href="https://translate.google.nl/?sl=auto&tl=ko&text=%EC%98%88%EC%9D%98.%0A%EC%97%BC%EC%B9%98.%0A%EC%9D%B8%EB%82%B4.%0A%EA%B7%B9%EA%B8%B0.%0A%EB%B0%B1%EC%A0%88%EB%B6%88%EA%B5%B4." target="_blank">De doelen van Taekwon-Do</a>
<a class="btn btn-outline-primary mr-3 mb-3" href="https://translate.google.nl/?sl=auto&tl=ko&text=%ED%95%98%EB%82%98.%0A%EB%91%98.%0A%EC%85%8B.%0A%EB%84%B7.%0A%EB%8B%A4%EC%84%AF.%0A%EC%97%AC%EC%84%AF.%0A%EC%9D%BC%EA%B3%B1.%0A%EC%97%AC%EB%8D%9F.%0A%EC%95%84%ED%99%89.%0A%EC%97%B4." target="_blank">Tellen</a>
<a class="btn btn-outline-primary mr-3 mb-3" href="https://translate.google.nl/?sl=auto&tl=ko&text=%EC%B0%A8%EB%A0%B7.%0A%EC%82%AC%EB%B2%94.%0A%EB%8B%98%EC%9E%AC.%0A%EA%B2%BD%EB%A1%80.%0A%ED%83%9C%EA%B6%8C!" target="_blank">Groeten</a>
<a class="btn btn-outline-primary mr-3 mb-3" href="https://translate.google.nl/?sl=auto&tl=ko&text=%EB%B6%80%EC%82%AC%EB%B2%94.%0A%EC%82%AC%EB%B2%94.%0A%EC%82%AC%ED%98%84.%0A%EC%82%AC%EC%84%B1." target="_blank">Titels</a>
<a class="btn btn-outline-primary mr-3 mb-3" href="https://translate.google.nl/?sl=auto&tl=ko&text=%ED%8B%80.%0A%EC%82%AC%EC%A3%BC%20%EC%B0%8C%EB%A5%B4%EA%B8%B0.%0A%EC%82%AC%EC%A3%BC%20%EB%A7%89%EA%B8%B0.%0A%EC%B2%9C%EC%A7%80.%0A%EB%8B%A8%EA%B5%B0.%0A%EB%8F%84%EC%82%B0.%0A%EC%9B%90%ED%9A%A8.%0A%EC%9C%A8%EA%B3%A1.%0A%EC%A4%91%EA%B5%B0.%0A%ED%87%B4%EA%B3%84.%0A%ED%99%94%EB%9E%91.%0A%EC%B6%A9%EB%AC%B4." target="_blank">Guptuls</a>
<a class="btn btn-outline-primary mr-3 mb-3" href="https://translate.google.nl/?sl=auto&tl=ko&text=%EA%B4%91%EA%B0%9C.%0A%ED%8F%AC%EC%9D%80.%0A%EA%B3%84%EB%B0%B1.%0A%EC%9D%98%EC%95%94.%0A%EC%B6%A9%EC%9E%A5.%0A%EA%B3%A0%EB%8B%B9.%0A%EC%A3%BC%EC%B2%B4.%0A%EC%82%BC%EC%9D%BC.%0A%EC%9C%A0%EC%8B%A0.%0A%EC%B5%9C%EC%98%81.%0A%EC%97%B0%EA%B0%9C.%0A%EC%9D%84%EC%A7%80.%0A%EB%AC%B8%EB%AC%B4.%0A%EC%84%9C%EC%82%B0.%0A%EC%84%B8%EC%A2%85.%0A%ED%86%B5%EC%9D%BC.&op=translate" target="_blank">Dantuls</a>
